package j2h.parser.tokens

import j2h.UnitSpec

/**
 * Created by bing on 2/13/15.
 */
class ArrayVariableTest extends UnitSpec{
  "ArrayVariable" should "return a Variable object when execute convertToNonArray" in {
    val a = new ArrayVariable(new Type("int",true),"a","i")
    a.convertToNonArray should be (new Variable(new Type("int"),"array_a0"))
  }
}
