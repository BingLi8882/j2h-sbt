package j2h.parser

import j2h.parser.tokens._
import j2h.{parser, UnitSpec}

/**
 * Created by bing on 3/21/15.
 */
class ParserTypeFinderTest extends UnitSpec{
  val source =
    """
      |int[] abc = 0;
      |for(int i =0;i<100;i++){
      |abc[100];
    """.stripMargin

  val p = parser.parseSource(source)

  "parser" should "parse variable correct" in {
    var t: Type = null
    assert(p.getJavaTokens(0).isInstanceOf[Type])
    t = p.getJavaTokens(0).asInstanceOf[Type]
    assert(t.name=="int")
    assert(t.isArray)

    var v: ArrayVariable = null

    assert(p.getJavaTokens(1).isInstanceOf[ArrayVariable])
    v = p.getJavaTokens(1).asInstanceOf[ArrayVariable]
    assert(v.name=="abc")
    assert(v.`type`.name=="int")

    var o: Operator = null

    assert(p.getJavaTokens(2).isInstanceOf[Operator])
    o = p.getJavaTokens(2).asInstanceOf[Operator]
    assert(o.name == "=")

    assert(p.getJavaTokens(4).isInstanceOf[Operator])
    o = p.getJavaTokens(4).asInstanceOf[Operator]
    assert(o.name == ";")


    var va: Value = null

    assert(p.getJavaTokens(3).isInstanceOf[Value])
    va = p.getJavaTokens(3).asInstanceOf[Value]
    assert(va.value == "0")

    //println(p.getJavaTokens(p.getJavaTokens.length-3))
    assert(p.getJavaTokens(p.getJavaTokens.length-3).isInstanceOf[ArrayVariable])
    v = p.getJavaTokens(p.getJavaTokens.length-3).asInstanceOf[ArrayVariable]
    assert(v.name=="abc")
    assert(v.`type`.name=="int")
    assert(v.`type`.isArray)

  }
}
