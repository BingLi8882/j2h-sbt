package j2h.parser

import j2h.parser.tokens._
import j2h.UnitSpec
import j2h.parser


/**
 * Created by bing on 3/4/15.
 */
class ParserTest extends UnitSpec {

  val source =
    """  .= 'a' 123.456 444 "aaaa"
      |//test test
      |//J2HSTART
      |/**
      |*aaaaaaa
      |*/
      |
      |//J2HEND
      |/*aaaaaaa*/
      |true false
      |class void
      |int[] double 123
      |String s
      |String[] array
      |class Abc;
      |abc meth1()
      |a.b.c.d()
      |abc()
    """.stripMargin
  val p = parser.parseSource(source)

  "Parser" should "parse operator token correct" in {
    var t: Operator = null

    assert(p.getJavaTokens(0).isInstanceOf[Operator])
    t = p.getJavaTokens(0).asInstanceOf[Operator]
    assert(t.name == ".")

    assert(p.getJavaTokens(1).isInstanceOf[Operator])
    t = p.getJavaTokens(1).asInstanceOf[Operator]
    assert(t.name == "=")

  }

  it should "parse Value token correct" in {
    var t: Value = null

    assert(p.getJavaTokens(2).isInstanceOf[Value])
    t = p.getJavaTokens(2).asInstanceOf[Value]
    assert(t.value == "'a'")

    assert(p.getJavaTokens(3).isInstanceOf[Value])
    t = p.getJavaTokens(3).asInstanceOf[Value]
    assert(t.value == "123.456")

    assert(p.getJavaTokens(4).isInstanceOf[Value])
    t = p.getJavaTokens(4).asInstanceOf[Value]
    assert(t.value == "444")

    assert(p.getJavaTokens(5).isInstanceOf[Value])
    t = p.getJavaTokens(5).asInstanceOf[Value]
    assert(t.value == "\"aaaa\"")
  }

  it should "parse Directive token correct" in {
    var t: Directive = null

    assert(p.getJavaTokens(6).isInstanceOf[Directive])
    t = p.getJavaTokens(6).asInstanceOf[Directive]
    assert(t.`type` == 0)
    assert(p.getJavaTokens(7).isInstanceOf[Directive])
    t = p.getJavaTokens(7).asInstanceOf[Directive]
    assert(t.`type` == 1)

  }
  it should "parse boolean value tokens correct" in {
    var t: Value = null

    assert(p.getJavaTokens(8).isInstanceOf[Value])
    t = p.getJavaTokens(8).asInstanceOf[Value]
    assert(t.value == "true")

    assert(p.getJavaTokens(9).isInstanceOf[Value])
    t = p.getJavaTokens(9).asInstanceOf[Value]
    assert(t.value == "false")
  }

  it should "parse key word correct" in {
    var t: Operator = null

    assert(p.getJavaTokens(10).isInstanceOf[Operator])
    t = p.getJavaTokens(10).asInstanceOf[Operator]
    assert(t.name == "class")

    assert(p.getJavaTokens(11).isInstanceOf[Operator])
    t = p.getJavaTokens(11).asInstanceOf[Operator]
    assert(t.name == "void")
  }
  it should "parse primitive types correct" in {
    var t: Type = null

    assert(p.getJavaTokens(12).isInstanceOf[Type])
    t = p.getJavaTokens(12).asInstanceOf[Type]
    assert(t.name == "int")
    assert(t.isArray)

    assert(p.getJavaTokens(13).isInstanceOf[Type])
    t = p.getJavaTokens(13).asInstanceOf[Type]
    assert(t.name == "double")
    assert(!t.isArray)
  }

  it should "parse Object types correct" in {
    var t: Type = null

    assert(p.getJavaTokens(15).isInstanceOf[Type])
    t = p.getJavaTokens(15).asInstanceOf[Type]
    assert(t.name == "String")
    assert(!t.isArray)

    assert(p.getJavaTokens(17).isInstanceOf[Type])
    t = p.getJavaTokens(17).asInstanceOf[Type]
    assert(t.name == "String")
    assert(t.isArray)
  }

  it should "declare variables correct" in {

    var t: Variable = null

    assert(p.getJavaTokens(16).isInstanceOf[Variable])
    t = p.getJavaTokens(16).asInstanceOf[Variable]
    assert(t.name == "s")
    assert(t.`type`.name == "String")

    var v: ArrayVariable = null
    assert(p.getJavaTokens(18).isInstanceOf[ArrayVariable])
    v = p.getJavaTokens(18).asInstanceOf[ArrayVariable]
    assert(v.name == "array")
    assert(v.`type`.name == "String")
    assert(v.`type`.isArray)

  }

  it should "declare class correct" in {
    var cl: ClassName = null
    var o: Operator = null
    assert(p.getJavaTokens(19).isInstanceOf[Operator])
    o = p.getJavaTokens(19).asInstanceOf[Operator]
    assert(o.name == "class")

    assert(p.getJavaTokens(20).isInstanceOf[ClassName])
    cl = p.getJavaTokens(20).asInstanceOf[ClassName]
    assert(cl.name == "Abc")

  }

  it should "parse method correct" in {
    var t: Type = null
    var m: Method = null

    assert(p.getJavaTokens(22).isInstanceOf[Type])
    t=p.getJavaTokens(22).asInstanceOf[Type]
    assert(t.name=="abc")

    assert(p.getJavaTokens(23).isInstanceOf[Method])
    m=p.getJavaTokens(23).asInstanceOf[Method]
    assert(m.name=="meth1")

    assert(p.getJavaTokens(26).isInstanceOf[Method])
    m=p.getJavaTokens(26).asInstanceOf[Method]
    assert(m.name=="a.b.c.d")
    assert(p.getJavaTokens(29).isInstanceOf[Method])
    m=p.getJavaTokens(29).asInstanceOf[Method]
    assert(m.name=="abc")
  }

  it should "parse EOF token correct" in {
    assert(p.getJavaTokens(p.getJavaTokens.length - 1).isInstanceOf[EOF])
  }
}
