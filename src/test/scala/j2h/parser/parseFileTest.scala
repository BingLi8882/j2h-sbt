package j2h.parser

import j2h.{parser, UnitSpec}

/**
 * Created by bing on 3/23/15.
 */
class parseFileTest extends UnitSpec{
  val source="""/************************
               |*
               |*Simple For Loop class
               |*
               |************************/
               |
               |package a.b.c.d;
               |
               |import java.io.IOException;
               |import java.util.StringTokenizer;
               |
               |public class Loop{
               |
               |  public String[] useless = new String[100];
               |  public char num;
               |
               |  /**
               |  * calculate t * 100000000 times
               |  */
               |  public int calculation(int base,int t){
               |    int result = base;
               |    for(int i=0;i<t;i++){
               |      for(int j=0;j<100000000;j++){
               |	if(j%2==0){
               |	  result *= 100;
               |	}else{
               |	  result /= 100;
               |	}
               |      }
               |    }
               |    return result;
               |  }
               |
               |  public void run(){
               |
               |    int[] a = new int[100];
               |    for(int i=0;i<100;i++){
               |      a[i] = i;
               |    }
               |    int num = 0;
               |
               |    //J2HSTART
               |    for(int i=0;i<100;i++){
               |      a[i] = calculation(a[i],1);
               |      String s = "aaaa";
               |    }
               |    //J2HEND
               |
               |    for(int i=0;i<100;i++){
               |      System.out.println(a[i]);
               |    }
               |  }
               |
               |  public static void main(String[] args){
               |    Loop loop = new Loop();
               |    loop.run();
               |  }
               |}
               |""".stripMargin

  val p = parser.parseSource(source)

  p.getJavaTokens.foreach(println _)
}
