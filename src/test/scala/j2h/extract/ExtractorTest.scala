package j2h.extract

import j2h.extractor.Extractor
import j2h.{parser, UnitSpec}

/**
 * Created by bing on 3/31/15.
 */
class ExtractorTest extends UnitSpec {
  val source = """package a.b.c.d;
                 |import java.io.IOException;
                 |import java.util.StringTokenizer;
                 |/**
                 | * Created by bing on 2/12/15.
                 | */
                 |public class Origin {
                 |
                 |    public int num = 10;
                 |
                 |
                 |    public void useless() {
                 |        num = 100;
                 |    }
                 |
                 |    public void loop() {
                 |        int[] a = new int[100];
                 |        for (int i = 0; i < 10; i++) {
                 |            a[i] = i;
                 |        }
                 |        //J2HSTART
                 |        for (int i = 0; i < 10; i++) {
                 |            a[i] += num;
                 |            a[i] += i;
                 |        }
                 |        //J2HEND
                 |        for (int i = 0; i < 10; i++) {
                 |            System.out.println(a[i]);
                 |        }
                 |    }
                 |
                 |    public static void main(String[] args){
                 |        Origin o = new Origin();
                 |        o.loop();
                 |    }
                 |
                 |}
                 |
                 | """.stripMargin

  val p = parser.parseSource(source)
  val e = new Extractor(p.getJavaTokens)

  "Extractor" should "Extract package name correct" in {
    e.packageName should be("package a.b.c.d;")
  }
  it should "extract imports correct" in {
    e.imports should be("\nimport java.io.IOException;\nimport java.util.StringTokenizer;")
  }
  it should "extract variables correct" in{
    e.variableList(0).name should be("array_a0")
    e.variableList(1).name should be("num")
    e.variableList(2).name should be("i")
  }
  it should "extract target loop correct" in{
    //e.getSource should be("array_a0+=num;\narray_a0+=i;\n")
    println(e.getSource)
  }
  it should "extract declaration correct" in{
    e.getDeclaration should be("int array_a0;\nint num;\nint i;\n")
    //println(e.getDeclaration)
  }
}
