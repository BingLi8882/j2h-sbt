package j2h.io.scanner

import j2h.UnitSpec

/**
 * Created by bing on 2/13/15.
 */
class ScannerTest extends UnitSpec {
  val s = "1111 0 1111.56 0.1 1. _num Num1 abac 'b' '_' '\\'' \"\" \" \\\" \" /*dadsa\n dadsa*/ //abcd\n +="
  val sc = new Scanner(s)

  "Scanner" should "scan numbers correct" in {
    sc.scan()
    sc.tokens(0).`type` should be(1)
    sc.tokens(0).value should be("1111")
    sc.tokens(1).`type` should be(1)
    sc.tokens(1).value should be("0")
    sc.tokens(2).`type` should be(1)
    sc.tokens(2).value should be("1111.56")
    sc.tokens(3).`type` should be(1)
    sc.tokens(3).value should be("0.1")
    sc.tokens(4).`type` should be(1)
    sc.tokens(4).value should be("1.")

  }

  it should "scan identifiers correct" in {
    sc.tokens(5).`type` should be(0)
    sc.tokens(5).value should be("_num")
    sc.tokens(6).`type` should be(0)
    sc.tokens(6).value should be("Num1")
    sc.tokens(7).`type` should be(0)
    sc.tokens(7).value should be("abac")

  }

  it should "scan chars correct" in {
    sc.tokens(8).`type` should be(1)
    sc.tokens(8).value should be("'b'")
    sc.tokens(9).`type` should be(1)
    sc.tokens(9).value should be("'_'")
    sc.tokens(10).`type` should be(1)
    sc.tokens(10).value should be("'\\''")

  }
  it should "scan strings correct" in {
    sc.tokens(11).`type` should be(1)
    sc.tokens(11).value should be("\"\"")
    sc.tokens(12).`type` should be(1)
    sc.tokens(12).value should be("\" \\\" \"")
  }
  it should "scan comments correct" in {
    sc.tokens(13).`type` should be(2)
    sc.tokens(13).value should be("/*dadsa\n dadsa*/")
    sc.tokens(14).`type` should be(2)
    sc.tokens(14).value should be("//abcd")
  }
  it should "scan operators correct" in {
    sc.tokens(15).`type` should be(3)
    sc.tokens(15).value should be("+")
    sc.tokens(16).`type` should be(3)
    sc.tokens(16).value should be("=")
  }

  it should "add EOF token at the end" in {
    sc.tokens(sc.tokens.length-1).`type` should be(Token.TOKEN_TYPES_EOF)
  }

}
