package j2h.database

import j2h.database
import j2h.UnitSpec

/**
 * Created by bing on 2/13/15.
 */
class ArrayNameBaseTest extends UnitSpec {
  "ArrayNameBase" should "return new index when the key not exists" in {
    database.getArrayIndex("i") should be(0)
  }

  it should "return the index in the Database when the key exists" in {
    database.getArrayIndex("i+1")
    database.getArrayIndex("i+1") should be(1)
  }
}
