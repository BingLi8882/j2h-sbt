import j2h.extractor.Extractor
import j2h.io.generator.Generator

/**
 * Created by bing on 2/17/15.
 */
/**
 * this is the Main object. is the entry of this project
 */
package object j2h {
  def main(args: Array[String]): Unit = {
    //val input = "Origin.java"
    //val output = "Loop.java"
    val extractor = new Extractor(parser.parseFile(args(0)).getJavaTokens)
    new Generator(extractor,args(1))

  }
}
