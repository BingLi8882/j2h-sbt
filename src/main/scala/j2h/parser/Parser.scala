package j2h.parser

import j2h.exceptions.WrongTokenTypeException
import j2h.io.scanner.Token
import j2h.parser.tokens._

/**
 * Created by bing on 2/24/15.
 */
/**
 * this class will analyze token list
 * @param token_list a list contains all tokens
 */
class Parser(token_list: List[Token]) {

  private var skip = 0
  private var className = ""


  /**
   * a list contains all tokens
   */
  private var java_tokens: List[TokenBase] = List()

  /**
   * retrieve parsing result
   * @return
   */
  def getJavaTokens = java_tokens

  /**
   * add one more token into token list
   * @param token a java token
   */
  private def add_token(token: TokenBase): Unit = {
    this.java_tokens :+= token
  }

  /**
   * Find type of specific variable from Java token list
   * @param name a String object represents the name of variable
   * @return s Type object represents the type of variable
   */
  private def typeFinder(name: String): Type = {
    //Using closure to count how many sub block need to be jump
    var closure = 0
    /**
     * Recursion function to find type
     * @param index an Int number represents current index
     * @return a Type object represent the type of variable
     */
    def finder(index: Int): Type = {


      if (index < 0) throw new IndexOutOfBoundsException("In Parser.typeFinder, can not find target variable " + name)
      else {
        this.java_tokens(index) match {
          case t: Operator => t.name match {
            case "}" =>
              closure += 1
            case "{" =>
              closure -= 1
            case ")" =>
              closure += 1
            case "(" =>
              closure -= 1
            case _ =>
              if (closure < 0) closure = 0
          }
            finder(index - 1)
          case v: Variable =>
            if (closure <= 0 && v.name == name) v.`type`
            else finder(index - 1)
          case av: ArrayVariable =>
            if (closure <= 0 && av.name == name) av.`type`
            else finder(index - 1)
          case _ => finder(index - 1)
        }
      }
    }
    finder(this.java_tokens.length - 1)
  }

  /**
   * If a comments Token is Directive, this method will convert this Token to Directive Token
   * and add it into token list
   * @param t a comments Token
   */
  private def parseDirective(t: Token): Unit = {
    if (t.`type` != Token.J2H_DIRECTIVE)
      throw new WrongTokenTypeException("The token type is " + t.`type` + " but expect type is " + Token.J2H_DIRECTIVE)
    if (t.value == Directive.getName(0)) add_token(new Directive(0))
    else if (t.value == Directive.getName(1)) add_token(new Directive(1))
  }

  /**
   * This method will retrieve the index of an array element
   * @param index the index in token list
   * @return a string value represents the index
   */
  private def getArrayIndex(index: Int): String = {
    skip += 1
    if (this.token_list(index).`type` == Token.TOKEN_TYPES_EOF) throw new IndexOutOfBoundsException("getArrayIndex method reach the end of token list")
    else if (this.token_list(index).value == "]") ""
    else this.token_list(index).value + getArrayIndex(index + 1)
  }

  /**
   * parse a Method
   * @param index the start index in token list
   */
  private def parseMethod(index: Int): Unit = {
    def getMethod(i: Int): String =
      if (this.token_list(i).value != ".") ""
      else {
        this.skip += 2
        this.token_list(i).value + this.token_list(i + 1).value + getMethod(i + 2)
      }

    add_token(new Method(this.token_list(index).value + getMethod(index + 1)))


  }

  /**
   * Parse a line until meet ;
   * @param index the start index
   * @return a string
   */
  private def parseLine(index: Int): String = {

    def next(i: Int): String = {
      if (token_list(i).value == ";") ""
      else {
        this.skip += 1
        token_list(i).value + next(i + 1)
      }
    }

    token_list(index).value + next(index + 1)
  }

  /**
   * Parse an identifier token to Method, Type, Operator or Variable token
   * @param index the index of target token
   */
  private def parseIdentifier(index: Int): Unit = {
    // parse boolean values to Value tokens
    if (tokens.BOOLEAN_VALUE contains token_list(index).value) add_token(new Value(token_list(index).value))
    // parse primitive type to type tokens
    else if (tokens.PRIMITIVE_TYPES contains token_list(index).value) {
      if (token_list(index + 1).value == "[" && token_list(index + 2).value == "]") {
        add_token(new Type(token_list(index).value, true))
        skip = 2
      } else add_token(new Type(token_list(index).value))
    }
    // parse key words to Operators tokens
    else if (tokens.KEY_WORDS contains token_list(index).value) add_token(new Operator(token_list(index).value))
    // declare a variable
    else if (java_tokens.last.isInstanceOf[Type]) {
      val t = this.java_tokens.last.asInstanceOf[Type]
      // parse method!!!!!!
      if (token_list(index + 1).value == "(") add_token(new Method(token_list(index).value))

      else if (t.isArray) add_token(new ArrayVariable(t, token_list(index).value))
      else add_token(new Variable(t, token_list(index).value))
    }
    // parse object types
    else if (token_list(index + 1).`type` == Token.TOKEN_TYPES_IDENTIFIER) add_token(new Type(token_list(index).value))
    // parse object arrays
    else if (token_list(index + 1).value == "[" && token_list(index + 2).value == "]") {
      add_token(new Type(token_list(index).value, true))
      skip = 2
    }
    //declare class
    else if (token_list(index - 1).value == "class") {
      add_token(new ClassName(token_list(index).value))
      this.className = token_list(index).value
    }
    //declare package
    else if (token_list(index - 1).value == "package") {
      add_token(new PackageName(this.parseLine(index)))
    }
    //parse imports
    else if (token_list(index - 1).value == "import") {
      add_token(new Imports(this.parseLine(index)))
    }
    // parse method!!!!!!
    else if (token_list(index + 1).value == "(" || token_list(index + 1).value == ".") this.parseMethod(index)
    else if (token_list(index - 1).value == "new") add_token(new Method(token_list(index).value))
    //parse array variables
    else if (token_list(index + 1).value == "[") {
      skip += 1
      add_token(new ArrayVariable(this.typeFinder(token_list(index).value), token_list(index).value, this.getArrayIndex(index + 2)))
    }
    // parse variables
    else add_token(new Variable(this.typeFinder(token_list(index).value), token_list(index).value))


  }

  /**
   * convert token list to java token list
   */
  private def parse(): Unit = {
    for (i <- 0 to (token_list.size - 1)) {
      if (skip > 0) skip -= 1
      else token_list(i).`type` match {
        case Token.TOKEN_TYPES_EOF => add_token(new EOF)
        case Token.TOKEN_TYPES_OPERATOR => add_token(new Operator(token_list(i).value))
        case Token.TOKEN_TYPES_IDENTIFIER => parseIdentifier(i)
        case Token.TOKEN_TYPES_VALUE => add_token(new Value(token_list(i).value))
        case Token.J2H_DIRECTIVE => this.parseDirective(token_list(i))

      }
    }
  }

  parse()
}

object Parser {
  def main(args: Array[String]): Unit = {
    val p = parseSource("")
    println(p.getJavaTokens.length)
  }

}
