package j2h.parser.tokens

/**
 * Created by bing on 2/26/15.
 */
/**
 * this class contains directive information
 * @param `type` the directive types
 */
case class Directive(`type`: Int) extends TokenBase(Directive.getName(`type`)){

  /**
   * override toString method
   * @return output format: //directive_name
   */
  override def toString = Directive.getName(this.`type`)

}

/**
 * this object contains all helper methods of Directive class
 */
object Directive {
  /**
   * represent J2HSTART
   */
  val START = 0
  /**
   * represent J2HEND
   */
  val END = 1

  /**
   * convert type to String
   * @param `type` directive type
   * @return a string represent directive
   */
  def getName(`type`: Int): String = if (`type` == 0) "//J2HSTART" else "//J2HEND"
}
