package j2h.parser.tokens

/**
 * Created by bing on 2/12/15.
 */
/**
 * this class contains value information (boolean, String, Char, Int, Float ...)
 * @param value s String object represents values
 */
case class Value(value: String) extends TokenBase(value){

  /**
   * override toString method
   * @return output format: value
   */
  override def toString = this.value
}
