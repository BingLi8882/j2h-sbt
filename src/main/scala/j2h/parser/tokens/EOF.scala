package j2h.parser.tokens

/**
 * Created by bing on 3/4/15.
 */
/**
 * this class represents a EOF token, only use at the end of token list
 */
case class EOF() extends TokenBase("EOF"){
}
