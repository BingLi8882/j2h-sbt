package j2h.parser.tokens

/**
 * Created by bing on 2/26/15.
 */
/**
 * this class contains type information
 * @param name the name of type
 * @param isArray whether this is array type
 */
case class Type(name: String, isArray: Boolean) extends TokenBase(name) {
  /**
   * constructor for non array types
   * @param name the name of type
   */
  def this(name: String) = this(name, false)

  /**
   * override toString method
   * @return output format: type_name
   */
  override def toString = if (isArray) this.name + "[]" else this.name
}
