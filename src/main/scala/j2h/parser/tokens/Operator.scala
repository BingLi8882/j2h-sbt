package j2h.parser.tokens

/**
 * Created by bing on 2/12/15.
 */
/**
 * this class contains operator information
 * @param name name of operator
 */
case class Operator(name: String) extends TokenBase(name){

  /**
   * override toString method
   * @return output format: operator_name
   */
  override def toString = this.name
}
