package j2h.parser.tokens

/**
 * Created by bing on 2/16/15.
 */
/**
 * this class contains method information
 * @param name the name of method
 */
case class Method(name: String) extends TokenBase(name){

  /**
   * override toString method
   * @return output format: classname.method name
   */
  override def toString = name

}
