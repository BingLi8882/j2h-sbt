package j2h.parser.tokens

/**
 * Created by bing on 3/31/15.
 */
/**
 * Class object represents the package name
 * @param name a String object represents class name
 */

class PackageName(val name: String) extends TokenBase(name) {

  /**
   * override toString method
   * @return the name of package
   */
  override def toString = name

}
