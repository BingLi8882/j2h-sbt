package j2h.parser

/**
 * Created by bing on 3/10/15.
 */
/**
 * this package object contains all Java reserve words
 */
package object tokens {
  /**
   * this List contains all boolean value
   **/
  val BOOLEAN_VALUE = List("true", "false")
  /**
   * this List contains all key word
   */
  val KEY_WORDS = List("assert", "break", "class", "const", "continue", "default", "do", "else", "enum", "extends",
    "final", "finally", "for", "goto", "if", "implements", "import", "instanceof", "interface", "native", "new",
    "package", "private", "protected", "public", "return", "static", "strictfp", "super", "switch", "synchronized",
    "this", "throw", "throws", "transient", "try", "void", "volatile", "while", "null", "case", "catch")
  /**
   * this List contains all java primitive types
   */
  val PRIMITIVE_TYPES = List("int", "double", "boolean", "char", "long", "short", "float")
}
