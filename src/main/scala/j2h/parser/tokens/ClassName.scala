package j2h.parser.tokens

/**
 * Created by bing on 3/23/15.
 */
/**
 * Class object represents the class name
 * @param name a String object represents class name
 */
class ClassName(val name: String) extends TokenBase(name) {
  /**
   * override toString method
   * @return the name of class
   */
  override def toString = name
}
