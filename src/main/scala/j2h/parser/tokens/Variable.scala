package j2h.parser.tokens

import j2h.database

/**
 * Created by bing on 2/12/15.
 */
/**
 * This class record the variable information
 * @param type a String value represent the type of this variable
 * @param name a String value represent the value of this variable
 */
case class Variable(`type`: Type, name: String) extends TokenBase(name) {

  /**
   * override toString method
   * @return output format: name
   */
  override def toString = this.name

  /**
   * get the declare instruction of this variable
   * @return output format: type variable_name
   */
  def declare = this.`type` + " " + this.name


}

/**
 * This class record the array variable information
 * @param `type` a String value represent the type of this variable
 * @param name a String value represent the value of this variable
 * @param index a String value represent the index of elements, "", if representing array
 */
case class ArrayVariable(`type`: Type, name: String, index: String) extends TokenBase(name) {

  /**
   * the constructor of ArrayVariable class without index argument
   * @param `type` a String value represent the type of this variable
   * @param name a String value represent the value of this variable
   * @return an ArrayVariable object
   */
  def this(`type`: Type, name: String) = this(`type`, name, "")

  /**
   * the full name of this array variable, name[index]
   */
  val fullName = this.name + "[" + this.index + "]"

  /**
   * convert current array element to non array variable
   * @return a Variable Object
   */
  def convertToNonArray = new Variable(new Type(this.`type`.name), "array_" + this.name + database.getArrayIndex(this.index))

  /**
   * override toString method
   * @return output format: variable_name[index]
   */
  override def toString =
    if (index != "") this.name + "[" + this.index + "]"
    else this.name


  /**
   * declare this array
   * @return output format: Array_type[] name
   */
  def declare = this.`type` + "[] " + this.name

}

/**
 * Variable object contains main and other help method of Variable class
 */
object Variable {
  /**
   * test method
   * @param args
   */
  def main(args: Array[String]): Unit = {

  }
}
