package j2h.parser.tokens

/**
 * Created by bing on 3/31/15.
 */
/**
 * Class object represents the imports name
 * @param name a String object represents class name
 */
class Imports (val name: String) extends TokenBase(name) {
  /**
   * override toString method
   * @return the name of class
   */
  override def toString = name
}
