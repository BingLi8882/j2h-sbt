package j2h

import j2h.io.scanner

import scala.io.Source

/**
 * Created by bing on 2/24/15.
 */
/**
 * contain all helper methods of parser
 */
package object parser {
  /**
   * create new parser object by giving path of source file
   * @param path the path of Java source file
   * @return a Parser object
   */
  def parseFile(path: String) = new Parser(scanner.scan(Source.fromFile(path).mkString))

  /**
   * create new parser object by giving source
   * @param source a String represent source
   * @return a Parser object
   */
  def parseSource(source: String) = new Parser(scanner.scan(source))
}
