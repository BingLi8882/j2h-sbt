package j2h.extractor

import j2h.exceptions.UnsupportedTypeException
import j2h.parser.tokens._

/**
 * Created by bing on 3/31/15.
 */
/**
 * This object will extract all useful information from given token list
 * @param tokens a java token list generated from Parser
 */
class Extractor(tokens: List[TokenBase]) {
  /**
   * get packageName declaration
   */
  var packageName: String = ""
  /**
   * get imports declaration
   */
  var imports: String = ""
  private var target: List[TokenBase] = List[TokenBase]()
  var variableList: List[Variable] = List[Variable]()
  /**
   * output process codes
   * @return a String object
   */
  private def getProcess:String={
    var s = ""
    this.target.foreach(e => {
      s += e.toString
      if (e.toString == ";") s += "\n"
    })
    s
  }

  /**
   * output input codes
   * @return a String object
   */
  private def getInput:String={
    var s = ""
    this.variableList.foreach(t=>{
      t.`type`.name match {
        case "int"=>
          s+=t.name+" = Integer.valueOf(tokenizer.nextToken());\n"
        case _ =>
          throw new UnsupportedTypeException("Unsupported Variable Type: "+t.`type`.name)
      }
    })
    s
  }

  /**
   * output output codes
   * @return a String object
   */
  private def getOutput:String={//__________________________
    var s = "output.collect(id, new Text(\"\""

    this.variableList.foreach(t=>{
      s+="+"+t.name+"+\" \""
    })

    s+"));"
  }

  /**
   * output source codes
   * @return a String object
   */
  def getSource: String = this.getInput+"\n"+this.getProcess+"\n"+this.getOutput+"\n"

  /**
   * output variables declarations
   * @return a String object
   */
  def getDeclaration:String={
    var s=""
      this.variableList.foreach(s+=_.declare+";\n")
    s
  }

  /**
   * add a variable into variable list
   * @param v a Variable object
   */
  private def addVariable(v: Variable): Unit =
    if (!this.variableList.exists(_.name == v.name)) this.variableList :+= v


  /**
   * Transform Loop to MapReduce
   * @param list target loop
   * @return mapreduce codes
   */
  private def loopTransform(list: List[TokenBase]): List[TokenBase] = {
    var nl: List[TokenBase] = List()
    var start = 0
    list.foreach(e => {
      if (start > 0) {
        // Loop body
        e match {
          case op: Operator =>
            op.name match {
              case "{" =>
                start += 1
                nl :+= op
              case "}" =>
                start -= 1
                if (start > 0) nl :+= op
              case _ =>
                nl :+= op
            }
          case array: ArrayVariable =>
            //println(array.fullName+"-----\n")
            val t = array.convertToNonArray
            nl :+= t
            this.addVariable(t)
          case va: Variable =>
            this.addVariable(va)
            nl :+= va
          case _ =>
            nl :+= e
        }
      }
      else {
        // for(){
        e match {
          case op: Operator =>
            if (op.name == "{") start += 1
          case _ =>
        }
      }
    }
    )
    nl
  }

  /**
   * Extract information from token list
   */
  def extract(): Unit = {
    var start = false
    tokens.foreach(_ match {
      case pn: PackageName => // extract package name
        this.packageName = "package " + pn + ";"
      case im: Imports => //extract imports
        this.imports += "\nimport " + im + ";"
      case dr: Directive => // detect directives
        if (dr.`type` == 0) start = true
        else start = false
      case t: TokenBase => // extract target loop
        if (start) this.target :+= t
      case _ =>
        throw new Exception("Unknown Token Type")
    })

    this.target = this.loopTransform(this.target)

  }

  extract()


}
