package j2h.exceptions

/**
 * Created by bing on 2/17/15.
 */
/**
 * This exceptions throws when scanner meets unknown tokens.
 * @param msg the exception report
 */
class UnknownTokenException (msg: String) extends RuntimeException(msg) {

}
