package j2h.exceptions

/**
 * Created by bing on 4/3/15.
 */
/**
 * This exception would be thrown when meet a unsupported variable type
 * @param msg
 */
class UnsupportedTypeException (msg: String) extends RuntimeException(msg) {

}
