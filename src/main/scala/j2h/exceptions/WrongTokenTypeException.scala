package j2h.exceptions

/**
 * Created by bing on 3/5/15.
 */
/**
 * The exception is thrown by Parser when meets an unexpected Token types
 * @param msg the exception report
 */
class WrongTokenTypeException(msg: String) extends RuntimeException(msg) {

}
