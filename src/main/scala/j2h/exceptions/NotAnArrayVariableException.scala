package j2h.exceptions

/**
 * Created by bing on 2/12/15.
 */
/**
 * This exceptions throws when try to operate a non array variables as an array variables.
 * Such as convertToNonArray method
 * @param msg the exception report
 */
class NotAnArrayVariableException(msg: String) extends RuntimeException(msg) {

}
