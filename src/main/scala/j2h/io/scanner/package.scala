package j2h.io

/**
 * Created by bing on 2/17/15.
 */
/**
 * scanner Object contains help method of Scanner package
 */
package object scanner {
  /**
   * Scan a source string and convert to Token list
   * @param source the source
   * @return a list contains all tokens
   */
  def scan(source: String): List[Token] = {
    val sc = new Scanner(source)
    sc.scan()
    sc.tokens
  }

  def main(args: Array[String]): Unit = {

    val s = "111 0 0.57 1.13 .5"
    val sc = new Scanner(s)


  }
}
