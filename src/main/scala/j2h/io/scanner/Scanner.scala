package j2h.io.scanner

import j2h.exceptions.UnknownTokenException

import scala.util.matching.Regex

/**
 * Created by bing on 2/13/15.
 */
/**
 * the main class of scanner package. create a scanner object and convert source code to parser tree
 * @param source a String value, read from java Source file
 */
class Scanner(source: String) {
  private var code = source
  var tokens = List[Token]()

  /**
   * get the first matched element from source and then remove it
   * @param r the pattern
   * @param t the type of Token
   * @return the the first matched token
   */
  private def getToken(r: Regex, t: Int): Token = {
    val token = new Token(t, r.findFirstIn(code).mkString)
    code = r.replaceFirstIn(code, "")
    token
  }

  /**
   * add giving token to token list
   * @param t a token object
   */
  private def addToken(t: Token): Unit = {
    this.tokens = this.tokens :+ t
  }

  /**
   * Scan source String and convert to Token list
   */
  def scan(): Unit = {
    if (code.length == 0) addToken(new Token())
    else {
      if (code(0).isDigit) {
        //scan numbers int and float
        addToken(getToken(new Regex("(0|[1-9]\\d*)(\\.\\d*)?"), Token.TOKEN_TYPES_VALUE))
        scan()
      }
      else if (code(0).isWhitespace) {
        //scan whitespace, scanner will ignore whitespace
        getToken(new Regex("\\s+"), Token.TOKEN_TYPES_VALUE)
        scan()
      }
      else if ((code(0).isLetter) || (code(0) == '_')) {
        // scan identifiers
        addToken(getToken(new Regex("\\w*"), Token.TOKEN_TYPES_IDENTIFIER))
        scan()
      }
      else if (code(0) == '\'') {
        // scan char
        addToken(getToken(new Regex("'(\\\\)?.'"), Token.TOKEN_TYPES_VALUE))
        scan()
      }
      else if (code(0) == '"') {
        // scan String
        addToken(getToken(new Regex("((\"\")|(\".*[^\\\\]\"))"), Token.TOKEN_TYPES_VALUE))
        scan()
      }
      else if ((code(0) == '/') && ((code(1) == '/') || (code(1) == '*'))) {
        // scan comments
        addToken(getToken(new Regex("^(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/)|^(//.*)"), Token.J2H_DIRECTIVE))
        scan()
      }
      else if (code(0).toString.matches("\\W")) {
        // scan operators
        addToken(getToken(new Regex("\\W"), Token.TOKEN_TYPES_OPERATOR))
        scan()
      }
      else throw new UnknownTokenException("Scanner meets an unknown token start with" + code(0)) //otherwise throw exception
    }

  }

  /**
   * override toString, print all elements in token list
   * @return
   */
  override def toString = {
    var s = ""
    tokens.foreach(s += _.toString)
    s
  }
}

