package j2h.io.scanner

/**
 * Created by bing on 2/11/15.
 */

/**
 * This class record the Token information
 * @param type a String variable represent the type of this token
 * @param value a String variable represent the value of this token
 */
class Token(val `type`: Int, val value: String) {

  /**
   * This constructor create an EOF token
   */
  def this() = this(Token.TOKEN_TYPES_EOF, ".")

  /**
   * Override toString method
   * @return a String value represent token's type and value
   */
  override def toString = this.`type` + " : " + this.value
}

/**
 * This object contains all token information
 */
object Token {

  //Token types
  /**
   * token types identifier
   */
  val TOKEN_TYPES_IDENTIFIER = 0
  /**
   * token types VALUE
   */
  val TOKEN_TYPES_VALUE = 1
  /**
   * token types operator
   */
  val TOKEN_TYPES_OPERATOR = 3

  /**
   * token types EOF
   */
  val TOKEN_TYPES_EOF = 4

  /**
   * token types directive J2HSTART and J2HEND
   */
  val J2H_DIRECTIVE = 2

  /**
   * this list contains all java key words
   */
  val KEY_WORDS = List("assert", "boolean", "break", "char", "class", "const", "continue", "default", "do",
    "double", "else", "enum", "extends", "final", "finally", "float", "for", "goto", "if", "implements", "import",
    "instanceof", "int", "interface", "long", "native", "new", "package", "private", "protected", "public", "return",
    "short", "static", "strictfp", "super", "switch", "synchronized", "this", "throw", "throws", "transient", "try",
    "void", "volatile", "while", "false", "null", "true", "case", "catch")

  /**
   * this list contains all java operator
   */
  val OPERATORS = List("+", "-", "*", "/", "%", "++", "--", "==", "!=", ">", "<", ">=", "<=", "&", "|", "^", "~", "<<",
    ">>", ">>>", "&&", "||", "!", "=", "+=", "-=", "*=", "/=", "%=", "<<=", ">>=", "&=", "^=", "|=", "(", ")", "[",
    "]", "{", "}", ".", "?", ":", ",", ";")
  /**
   * this list contains all java white space
   */
  val WHITE_SPACES = List(" ", "\t", "\n", "\r")



  /**
   * Main method is used for test only
   * @param args
   */
  def main(args: Array[String]) {
    val t = new Token()

  }
}