package j2h.io.generator

import j2h.extractor.Extractor

/**
 * Created by bing on 4/3/15.
 */
class Generator(extractor: Extractor, path: String) {
  val imports =
    """
      |import java.io.IOException;
      |import java.util.StringTokenizer;
      |import org.apache.hadoop.fs.Path;
      |import org.apache.hadoop.io.IntWritable;
      |import org.apache.hadoop.io.LongWritable;
      |import org.apache.hadoop.io.Text;
      |import org.apache.hadoop.mapred.FileInputFormat;
      |import org.apache.hadoop.mapred.FileOutputFormat;
      |import org.apache.hadoop.mapred.JobClient;
      |import org.apache.hadoop.mapred.JobConf;
      |import org.apache.hadoop.mapred.MapReduceBase;
      |import org.apache.hadoop.mapred.Mapper;
      |import org.apache.hadoop.mapred.OutputCollector;
      |import org.apache.hadoop.mapred.Reporter;
      |import org.apache.hadoop.mapred.TextOutputFormat;
    """.stripMargin

  val part1 =
    """
      |public class Loop {
      |
      |    public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, Text> {
      |
      |        @Override
      |        public void map(LongWritable k1, Text v1, OutputCollector<IntWritable, Text> output, Reporter rprtr) throws IOException {
      |            String input = v1.toString();
      |            StringTokenizer tokenizer = new StringTokenizer(input);
      |            IntWritable id;
    """.stripMargin

  val part2 =
    """
      |  while(tokenizer.hasMoreTokens()){
      |
      |                id = new IntWritable(Integer.valueOf(tokenizer.nextToken()));
    """.stripMargin
  val part3 =
    """
      |  }
      |
      |        }
      |    }
      |
      |
      |    public static void main(String[] args)throws Exception{
      |        JobConf conf = new JobConf(Loop.class);
      |        conf.setJobName("Calculator");
      |
      |        conf.setOutputKeyClass(IntWritable.class);
      |        conf.setOutputValueClass(Text.class);
      |
      |        conf.setMapperClass(Map.class);
      |        //conf.setCombinerClass(Reduce.class);
      |        //conf.setReducerClass(Reduce.class);
      |
      |        conf.setInputFormat(NoSplitInputFormat.class);
      |        conf.setOutputFormat(TextOutputFormat.class);
      |
      |        if(args.length>=3)conf.setNumReduceTasks(Integer.valueOf(args[2]));
      |        else conf.setNumReduceTasks(1);
      |        FileInputFormat.setInputPaths(conf, new Path(args[0]));
      |        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
      |
      |        JobClient.runJob(conf);
      |    }
      |
      |
      |}
    """.stripMargin

  val out = new java.io.FileWriter(path)
  out.write(extractor.packageName + "\n" + extractor.imports + "\n" + this.imports + "\n"
    + this.part1 + "\n" + extractor.getDeclaration + "\n" + this.part2 + "\n"
    + extractor.getSource + "\n"+this.part3)
  out.close()

}
