package j2h.io.generator

import java.io.PrintWriter

import consoleGUI.components.PercentageBarWithMessage

/**
 * Created by bing on 2/18/15.
 */
/**
 * only use to generator test data
 */
object TestDataGenerator {

  val pb = new PercentageBarWithMessage
  val filename = "file"
  //modify size
  val size = 100000000l


  def main(args: Array[String]): Unit = {
    val num = Integer.valueOf(args(0))
    var index = 0
    var p = 0
    var printer = new PrintWriter(filename + " " + index)
    pb.message("create file" + filename + index)
    pb.changeValue(0)
    (1l to size).foreach(i => {
      if ((index + 1.0) / num * size < i) {
        printer.close()
        pb.message(filename + index + " complete")
        printer = new PrintWriter(filename + " " + index)
        index += 1
        pb.message("create file" + filename + " " + index)
      }
      printer.print(i + " " + 10 + " " + 10 + " " + 10 + "\n")
      if ((i * 100.0 / size).toInt > p) {
        p += 1
        pb.changeValue(p)
      }
    })
    printer.close()
    pb.message(filename + index + " complete")
    pb.finish()

  }

}
