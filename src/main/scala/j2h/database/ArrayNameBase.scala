package j2h.database

/**
 * Created by bing on 2/12/15.
 */
/**
 * a database contain array index information
 */
class ArrayNameBase {

  private var index = -1
  private var db = Map[String, Int]()

  /**
   * get the index of giving key
   * @param key the key
   * @return the index in database if found, otherwise, new index
   */
  def apply(key: String) = db.getOrElse(key, addElement(key))

  /**
   * add new element in database
   * @param key the key
   * @return return the value
   */
  def addElement(key: String) = {
    index += 1
    db += (key -> index)
    index
  }

  /**
   * override toString method
   * @return a String value represent the contains of database
   */
  override def toString = db.toString
}


