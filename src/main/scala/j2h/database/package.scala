package j2h

/**
 * Created by bing on 2/17/15.
 */
/**
 * this object contains all operations of Database
 */
package object database {
  private val arrayIndexDB = new ArrayNameBase()

  /**
   * get the index of an array element
   * @param key index of giving array element
   * @return the index in database
   */
  def getArrayIndex(key: String) = this.arrayIndexDB(key)


  def main(args: Array[String]) = {
    println(getArrayIndex("i"))
    println(getArrayIndex("i+1"))
    println(getArrayIndex("i-1"))

    println(getArrayIndex("i"))
    println(getArrayIndex("i+1"))
    println(getArrayIndex("i-1"))

  }
}
