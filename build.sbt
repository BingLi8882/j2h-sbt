name := "j2h"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"

libraryDependencies += "org.scala-lang" % "scala-library" % "2.11.6"

mainClass in (Compile, packageBin) := Some("j2h.package")

mainClass in assembly := Some("j2h.package")

test in assembly := {}
    